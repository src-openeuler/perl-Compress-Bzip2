Name:           perl-Compress-Bzip2
Version:        2.28
Release:        2
Summary:        This module provides a Compress::Zlib like Perl interface to the bzip2 library
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/Compress-Bzip2
Source0:        https://cpan.metacpan.org/authors/id/R/RU/RURBAN/Compress-Bzip2-%{version}.tar.gz
BuildRequires:  findutils gcc make bzip2-devel >= 1.0.5 sed perl(ExtUtils::MakeMaker)
BuildRequires:  perl-interpreter perl-generators perl(Test::More) perl-devel
Requires:       perl(constant) >= 1.04

%{?perl_default_filter}

%description
This module provides a Compress::Zlib like Perl interface to the bzip2 library.
It uses the low level interface to the bzip2 algorithm, and reimplements all high
level routines.

%package_help

%prep
%setup -q -n Compress-Bzip2-%{version}

find bzlib-src -mindepth 1 -type f \! -name 'sample*' -delete
sed -i -e '/^bzlib-src\//d' MANIFEST
find bzlib-src -type f >>MANIFEST

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PERLLOCAL=1 NO_PACKLIST=1 OPTIMIZE="%{optflags}"
%make_build

%install
%make_install
%{_fixperms} %{buildroot}

%check
make test

%files
%license COPYING
%doc README.md
%{perl_vendorarch}/Compress/
%{perl_vendorarch}/auto/Compress/

%files help
%doc ANNOUNCE Changes NEWS
%{_mandir}/man3/*.3pm*

%changelog
* Tue Jan 14 2025 Funda Wang <fundawang@yeah.net> - 2.28-2
- drop useless perl(:MODULE_COMPAT) requirement

* Wed Jul 12 2023 leeffo <liweiganga@uniontech.com> - 2.28-1
- upgrade to version 2.28

* Wed May 13 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.26-11
- Add build requires of perl-devel

* Wed Jan 8 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.26-10
- Package init
